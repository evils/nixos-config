{ config, pkgs, ...}: {

  imports = [ <musnix> ];
  musnix.enable = true;
  musnix.rtcqs.enable = true;
  users.users.evils.extraGroups = [ "audio" ];

  environment.systemPackages = with pkgs; [
    pavucontrol
  ];

  #sound.enable = true;
  nixpkgs.config.pulseaudio = true;

  # some of the suggested settings for real time audio from the nixos wiki page on pipewire
  security.rtkit.enable = true;
  services.pipewire = {
    enable = true;
    alsa.enable = true;
    alsa.support32Bit = true;
    pulse.enable = true;
    jack.enable = true;

    extraConfig = {
      pipewire.context.properties = {
        default.clock.rate = 192000;
        default.clock.quantum = 512;
        default.clock.min-quantum = 32;
        default.clock.max-quantum = 512;
      };
      jack.properties.node.latency = "512/192000";
    };
  };
}
