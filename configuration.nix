# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }: {

  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
      # and my own stuff
      ./i3.nix
      ./sway.nix
      ./pkgs.nix
      ./zsh.nix
      ./libvirt.nix
      ./nur.nix
      ./desktop.nix
      ./udev.nix
      ./telegraf.nix
      ./sysstat.nix
#      ./wireshark.nix
#      ./beesd.nix
#      ./rasdaemon.nix
#      ./eid.nix
#      ./tftpd.nix
#      ./eaton.nix
      ./bt.nix
      ./audio.nix
      ./print-scan.nix

      ./electron.nix
      ./kernel-serial.nix
    ];

  hardware.rasdaemon = {
    enable = true;
    mainboard = ''
      vendor = ASRock
      model = B450M Pro4
    '';
    labels = ''
      vendor: ASRock
        product: To Be Filled By O.E.M.
        model: B450M Pro4
          DDR4_A1: 0.2.0;  DDR4_A2: 0.2.1;
          DDR4_B1: 0.3.0;  DDR4_B2: 0.3.1;
    '';
  };

  hardware.graphics = {
    enable = true;
    extraPackages = with pkgs; [
      rocmPackages.clr.icd
      rocmPackages.rocm-runtime

      amdvlk
    ];
  };

  hardware.fancontrol = {
    enable = true;
    config = builtins.readFile /home/evils/.config/fancontrol;
  };

  hardware.spacenavd.enable = true;

#  powerManagement.cpuFreqGovernor = "schedutil"; # conflicts with musnix's "performance"

  services.tuptime.enable = true;

  services.clarissa = {
    enable = true;
    interface = "enp5s0";
  };

  fileSystems."/media/bulk".options = [ "compress=zstd" "nofail" ];
  services.fstrim.enable = true;

  boot.kernelPackages = pkgs.linuxPackages_latest;
  environment.systemPackages = [ pkgs.linuxPackages_latest.turbostat ];

  boot.initrd.availableKernelModules = [ "uas" ]; # to allow this system to boot from an USB NVMe enclosure
  
  boot.initrd.kernelModules = [
    # beeping computer...
    "pcspkr"
    # for sensors
    "nct6775" "jc42"
    # for i2c-tools' decode-dimms
    "at24"
    # "usbmon"
    # for turbostat
    #"msr"

    # maybe this solves my usb issues
    "xhci-pci-renesas"
  ];
  
  boot.loader.timeout = 2;
  nixpkgs.config.allowUnfree = true; # for memtest86 because our memtest86+ does not do EFI yet
  boot.loader.systemd-boot = {
    enable = true;
    memtest86.enable = true;
  };
  
  networking = {
    hostName = "valix";
    networkmanager.enable = true;
    usePredictableInterfaceNames = true;
    nameservers = [
      "1.1.1.1"
      #"10.20.0.253" # doesn't work to discover hostnames over VPN
    ];
  };
  programs.nm-applet.enable = true;

# Select internationalisation properties.
  i18n.defaultLocale = "en_US.UTF-8";

  console.font = "Lat2-Terminus16";
  console.keyMap = "dvorak";


# Set your time zone.
  time.timeZone = "Europe/Brussels";

  environment.pathsToLink = [ "/libexec" ];
  environment.enableDebugInfo = true;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.settings.PasswordAuthentication = false;

  services.smartd.enable = true;

  services.vnstat.enable = true;
  environment.etc."vnstat.conf".text = ''
    MonthRotate 25
  '';

  virtualisation = {
#    virtualbox.host.enable = true;
    libvirtd.enable = true;
  };

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.users.evils = {
    isNormalUser = true;
    shell = pkgs.zsh;
    extraGroups = [ "wheel" "audio" "video" "dialout" "input" "beep" "embedded" "plugdev" ];
    openssh.authorizedKeys.keys = [ "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsf4/CPraa6qKX3cGuQ6pXhe5XbWj8UVQy1YCAcdCyYgnlWeeRF7fegTsDA2nwzW9Vq+iLHG5O6/XqHrahajyQcFATOPpMvlcpww1btgg+CVnZBw+GQPH6vM8NtbipNCegqvYpAqAH1adLlppyJ3vQWt69pSw20cOcsgij8mp3FT5c3wJ0PYMArSmGtwGunJAv2BumxxCxIEnwskYwrTy4joDMke8BQjXdDNsQbjysJ/sLy9Lfk4AWTdy7VKVGb08PZpcRkWZNutMRdk06aPbIdVitRlWSnQ54Q94LDVS0CF2eGumlU+N9FN9RtffdOIynvFlBGGBGJzjFgO7L1Ph9 evils@evils-desktop" ];
  };

  fonts = {
    fontDir.enable = true; # may be for x11 only
    fontconfig.defaultFonts.monospace = [ "Noto Sans Mono" ];
    packages = with pkgs; [
      noto-fonts
      noto-fonts-extra
      noto-fonts-emoji
      noto-fonts-cjk-sans

      vegur # the official NixOS font
    ];
  };

  documentation.dev.enable = true;

  services.fwupd.enable = true;
  services.lorri.enable = false; # temporarily disabled because it's broken by Rust

  nix = {
    settings.auto-optimise-store = true;
    gc = {
      automatic = true;
      dates = "Sat *-*-* 03:15:00";
    };
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "19.09"; # Did you read the comment?

}
