{ pkgs, ...}: {

  users.groups = {
    beep = { };
    embedded = { };
    plugdev = { };
  };

  services.udev = {
    packages = with pkgs; [
      stlink
      platformio
      qmk-udev-rules
    ];

    extraRules = ''
      # for liquidctl (NZXT e500)
      SUBSYSTEM=="hidraw", SUBSYSTEMS=="usb", ATTRS{idVendor}=="7793", ATTRS{idProduct}=="5911", MODE="0666"

      # beep
      ACTION=="add", SUBSYSTEM=="input", ATTRS{name}=="PC Speaker", ENV{DEVNAME}!="", GROUP="beep", MODE="0620"

      # Maple with DFU (blue pill roger clark USB DFU bootloader as well) (from the platformio udev rules)
      ATTRS{idVendor}=="1eaf", ATTRS{idProduct}=="000[34]", MODE:="0666", ENV{ID_MM_DEVICE_IGNORE}="1", ENV{ID_MM_PORT_IGNORE}="1", GROUP="embedded"

      # davidgfnet stm32-dfu-bootloader
      ATTRS{idVendor}=="dead", ATTRS{idProduct}=="ca5d", GROUP="embedded"

      # brymen bm869s
      ATTRS{idVendor}=="0820", ATTRS{idProduct}=="0001", GROUP="embedded"

      # black magic probe
      SUBSYSTEM=="tty", ATTRS{interface}=="Black Magic GDB Server", SYMLINK+="ttyBmpGdb"
      SUBSYSTEM=="tty", ATTRS{interface}=="Black Magic UART Port",SYMLINK+="ttyBmpTarg"

      # brother (label printers etc
      ATTRS{idVendor}=="04f9", GROUP="plugdev"


      # 71-nrf.rules
      ACTION!="add", SUBSYSTEM!="usb_device", GOTO="nrf_rules_end"

      # Set /dev/bus/usb/*/* as read-write for all users (0666) for Nordic Semiconductor devices
      SUBSYSTEM=="usb", ATTRS{idVendor}=="1915", MODE="0666"

      # Flag USB CDC ACM devices, handled later in 99-mm-nrf-blacklist.rules
      # Set USB CDC ACM devnodes as read-write for all users
      KERNEL=="ttyACM[0-9]*", SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idVendor}=="1915", MODE="0666", ENV{NRF_CDC_ACM}="1"

      LABEL="nrf_rules_end"

      # 99-modemmmanager-acm-fix.rules
      # Previously flagged nRF USB CDC ACM devices shall be ignored by ModemManager
      ENV{NRF_CDC_ACM}=="1", ENV{ID_MM_CANDIDATE}="0", ENV{ID_MM_DEVICE_IGNORE}="1"
    '';
  };

}
