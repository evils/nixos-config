_: {

  programs.zsh = {
    enable = true;
    autosuggestions = {
      enable = true;
      strategy = [ "match_prev_cmd" ];
    };
    enableCompletion = true;
    syntaxHighlighting = {
      enable = true;
    };
    shellAliases = {
      "btrfs" = "sudo btrfs";
      "weather" = "curl -Ss \"https://wttr.in/?0&Q\"";
      "haste" = "haste 2> /dev/null";
    };
    ohMyZsh.enable = true;
    ohMyZsh.theme = "evils";
    interactiveShellInit = ''
      ZSH_DISABLE_COMPFIX=true
      export ZSH=$HOME/.oh-my-zsh
      source $ZSH/oh-my-zsh.sh
      ZSH_CUSTOM=$ZSH/custom/
      export NIX_PAGER=cat
      export DIRENV_LOG_FORMAT=""
      eval "$(direnv hook zsh)"
    '';
    promptInit = "";
  };
}
