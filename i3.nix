_: {

  services.xserver = {
    enable = true;
    xkb = {
      layout = "us,us";
      variant = "dvorak,";
      options = "grp:shifts_toggle,lv3:ralt_switch,compose:ralt";
    };

    xautolock.time = 360;

/*

    xrandrHeads = [
        {
          output = "DisplayPort-1";
          monitorConfig =
            ''
              # Option "Rotate" "right"
              Option "LeftOf" "DisplayPort-2"
            '';
        }

        {
          output = "DisplayPort-2";
          primary = true;
        }

        {
          output = "DisplayPort-0";
          monitorConfig =
            ''
              Option "Rotate" "left"
              Option "RightOf" "DisplayPort-2"
            '';
        }
      ];

#    displayManager.startx.enable = true;

    windowManager.i3 = {
      enable = false;
      extraPackages = with pkgs; [
        dmenu
        i3status
        i3blocks
        i3lock
      ];
    };

*/

/*
    windowManager.matwm2 = {
      enable = true;
      config = ''
        background blue
        key mod1 d exec dmenu_run
      '';
      extraPackages = with pkgs; [
        dmenu
        i3status
        i3blocks
        i3lock
      ];
    };
*/

    videoDrivers = [ "pkgs.linuxPackages_latest.amdgpu-pro" "amdgpu" "mesa" "vulkan" ];
  };

}
