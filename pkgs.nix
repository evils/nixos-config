{ pkgs, ...}: {

  environment.systemPackages = with pkgs; [
    # system utilities
    gcc git zsh tree
    btrfs-progs
    edac-utils usbutils pciutils moreutils lshw lsof dmidecode
    smartmontools hddtemp lm_sensors htop
    curl wget
    # informational
    arp-scan clar
    # comforts
    numlockx
    cntr
  ];

  environment.enableAllTerminfo = true;

  services.ratbagd.enable = true;

  programs = {
    vim = {
      enable = true;
      defaultEditor = true;
    };
    gnupg.agent = {
      enable = true;
#      pinentryFlavor = "curses";
    };
    usbtop.enable = true;
  };

#  programs.wshowkeys.enable = true;

  programs.adb.enable = true;
  users.users.evils.extraGroups = [ "adbusers" ];
}
