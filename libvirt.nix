# from https://github.com/colemickens/nixcfg/blob/master/modules/mixin-libvirt.nix

{ pkgs, ... }:

{
  virtualisation.libvirtd.enable = true;
  virtualisation.spiceUSBRedirection.enable = true;

  users.users.evils.extraGroups = [ "libvirtd" ];

  environment.systemPackages = with pkgs; [
    bridge-utils
    virt-viewer
    virt-manager

    # workaround for usb, may get fixed
    # may have been fixed with spiceUSBRedirection option above
    #spice-gtk
  ];
}
