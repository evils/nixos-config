{ pkgs, ...}: {

# recreation of rasdaemon-edac, -mce and -aer tests
  hardware.rasdaemon.enable = true;
  boot.kernelPatches = [{
    name = "rasdaemon-tests";
    patch = null;
    extraConfig = ''
      EDAC_DEBUG y
      X86_MCE_INJECT y

      PCIEPORTBUS y
      PCIEAER y
      PCIEAER_INJECT y
    '';
  }];
  boot.initrd.kernelModules = [
    "edac_core"
    "amd64_edac_mod"
    "mce-inject"
    "aer-inject"
  ];
  environment.systemPackages = with pkgs; [
    rasdaemon
    error-inject.edac-inject
    error-inject.mce-inject
    error-inject.aer-inject
  ];
}
