# from andi-

# make sure all the electron apps that I am using are using wayland by default
{
  nixpkgs.overlays = [
    (self: super: {
      electronForceWayland = drv: binaryName: drv.overrideAttrs ({ postFixup ? "", nativeBuildInputs ? [ ], ... }: {
        nativeBuildInputs = nativeBuildInputs ++ [ self.makeWrapper ];
        postFixup = postFixup + ''
          wrapProgram $out/bin/${binaryName} \
            --add-flags "--enable-features=UseOzonePlatform" \
            --add-flags "--ozone-platform=wayland"
        '';
      });

      element-desktop = self.electronForceWayland super.element-desktop "element-desktop";
      # signal doesn't even open :(
      #signal-desktop = self.electronForceWayland super.signal-desktop "signal-desktop";
    })
  ];
}
