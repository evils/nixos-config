_: {

  services.beesd.filesystems = {
    bulk = { 
      spec = "LABEL=bulk";
      hashTableSizeMB = 4096;
      verbosity = 7;
    };
  };
}
