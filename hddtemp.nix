_: {

  hardware.sensor.hddtemp = {
    enable = true;
    drives = [
      "/dev/disk/by-id/ata-*"
    ];
  };

  # this fails because of permissions...
  services.telegraf.extraConfig."inputs"."hddtemp" = {};

}
