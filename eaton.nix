_: {

  power.ups = {
    enable = true;
    ups.eaton = {
      description = "eaton ellipse pro 1600 iec";
      summary = "eaton via hsg";
      driver = "usbhid-ups";
      port = "auto";
    };
  };

  # i think this is my eaton...
  services.udev.extraRules = ''
    SUBSYSTEM=="hidraw", SUBSYSTEMS=="usb", ATTRS{idVendor}=="0463", ATTRS{idProduct}=="ffff", GROUP="ups", MODE="0660"
  '';

}
