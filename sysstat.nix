{ pkgs, ...}: {

  services.sysstat.enable = true;

  services.telegraf.extraConfig."inputs"."sysstat" = {
    # otherwise it's named "enp5s0"
    "name_override" = "sysstat";
    "sadc_path" = "${pkgs.sysstat}/lib/sa/sadc";
    "sadf_path" = "${pkgs.sysstat}/bin/sadf";
    "group" = true;
    "options" = {
      "-n DEV --iface=enp5s0" = "enp5s0";
    };
  };

}
