{ pkgs, ... }: {

  # from https://discourse.nixos.org/t/desktop-oriented-kernel-scheduler/12588

  boot.kernel.sysctl = {
    #"vm.swappiness" = 90; # when swapping to ssd, otherwise change to 1 # conflicts with musnix's 10
    "vm.vfs_cache_pressure" = 50;
    "vm.dirty_background_ratio" = 20;
    "vm.dirty_ratio" = 50;
    # these are the zen-kernel tweaks to CFS defaults (mostly)
    "kernel.sched_latency_ns" = 4000000;
    # should be one-eighth of sched_latency (this ratio is not
    # configurable, apparently -- so while zen changes that to
    # one-tenth, we cannot):
    "kernel.sched_min_granularity_ns" = 500000;
    "kernel.sched_wakeup_granularity_ns" = 50000;
    "kernel.sched_migration_cost_ns" = 250000;
    "kernel.sched_cfs_bandwidth_slice_us" = 3000;
    "kernel.sched_nr_migrate" = 128;
  };

  # Tell kernel to use cgroups_v2 exclusively
  boot.kernelParams = [ "cgroup_no_v1=all" "systemd.unified_cgroup_hierarchy=yes" ];
  # Let users put things in appropriately-weighted scopes
  systemd = {
    extraConfig = ''
      DefaultCPUAccounting=yes
      DefaultMemoryAccounting=yes
      DefaultIOAccounting=yes
    '';
    user.extraConfig = ''
      DefaultCPUAccounting=yes
      DefaultMemoryAccounting=yes
      DefaultIOAccounting=yes
    '';
    services."user@".serviceConfig.Delegate = true;
  };

  systemd.services.nix-daemon.serviceConfig = {
    CPUWeight = 20;
    IOWeight = 20;
  };

  environment.systemPackages = [
    (
      pkgs.runCommand "nicely" {} ''
        mkdir -p $out/bin
        echo "#!/bin/sh -f" > $out/bin/nicely
        echo "exec systemd-run --user --quiet --scope --same-dir --collect -p CPUWeight=20 -p IOWeight=20 \"\$@\"" >> $out/bin/nicely
        chmod +x $out/bin/nicely
      ''
    )
  ];

}
