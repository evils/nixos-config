{ pkgs, ... }: {

  services.displayManager.defaultSession = "sway";

  services.xserver.displayManager.setupCommands = ''
    setxkbmap dvorak
    loadkeys dvorak
  '';

  services.displayManager.sddm = {
    enable = true;
    autoNumlock = true;
  };

  /*
  services.xserver.displayManager.gdm = {
    enable = true;
    wayland = true;
  };
  */

  programs.sway = {
    enable = true;
    extraPackages = with pkgs; [
      # defining something here removes the defaults?

      # legacy
      xwayland

      # wayland native
      swaylock swayidle
      bemenu
      alacritty
      # shouldn't be needed with waybar.enable = true?
      waybar
      sway-contrib.grimshot
      # wayland alternative to dunst
      mako
    ];
    extraSessionCommands = ''
      # for REW and maybe st-link's update utility
      export _JAVA_AWT_WM_NONREPARENTING=1
    '';
  };
  #programs.waybar.enable = true; # sway runs this itself, enabling it results in no loaded config

}
