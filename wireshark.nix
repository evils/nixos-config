# from https://github.com/cleverca22/nixos-configs/blob/master/wireshark-no-root.nix

{ pkgs, ... }:

{
  boot.initrd.kernelModules = [ "usbmon" ];

  users.users.evils.extraGroups = [ "wireshark" ];
  users.groups.wireshark.gid = 500;

  security.wrappers.dumpcap = {
    source = "${pkgs.wireshark}/bin/dumpcap";
    permissions = "u+xs,g+x";
    owner = "root";
    group = "wireshark";
  };

  programs.wireshark.enable = true;
}
