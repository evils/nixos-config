{
  # motherboard serial port, enabling that results in instability
  #boot.kernelParams = [ "console=ttyS0,115200n8" ];
  # tty used by sway
  boot.kernelParams = [ "console=tty0" ];
}
