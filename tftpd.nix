_: {

#  services.tftpd = {
#    enable = true;
#    path = "/home/evils/test/tftpd_root";
#  };

  services.atftpd = {
    enable = true;
    # permission issue in my home directory? default works
    #root = "/home/evils/test/tftpd_root";
  };

  networking.firewall = {
    allowedUDPPorts = [ 69 ];
  };

}
