{ pkgs, ... }: {

  services.telegraf = {
    enable = true;
    # contains a line like INFLUX_TOKEN=Tml4T1MgcnVsZXMgCg==
    environmentFiles = [ "/home/evils/.config/telegraf.env" ];
    extraConfig = {
      "agent" = {
        "interval" = "10s";
        "round_interval" = true;
        "metric_batch_size" = 1000;
        "metric_buffer_limit" = 10000;
        "collection_jitter" = "0s";
        "flush_interval" = "10s";
        "flush_jitter" = "0s";
        "precision" = "";
        "debug" = false;
        "quiet" = false;
        "logfile" = "";
        "hostname" = "";
        "omit_hostname" = false;
      };
      "outputs" = {
        "influxdb_v2" = [
          {
            "urls" = [ "http://influxdb:8086" ];
            "token" = "$INFLUX_TOKEN";
            "organization" = "home";
            "bucket" = "valix";
          }
        ];
      };
      "inputs" = {
        "cpu" = {
          "percpu" = true;
          "totalcpu" = true;
          "collect_cpu_time" = false;
          "report_active" = false;
        };
        "disk" = {
          "ignore_fs" = [
            "tmpfs"
            "devtmpfs"
            "devfs"
            "overlay"
            "aufs"
            "squashfs"
          ];
        };
        "diskio" = { };
        "mem" = { };
        "processes" = { };
        "swap" = { };
        "system" = { };
        "kernel" = { };
        "ras" = { };
        "amd_rocm_smi" = {
          "bin_path" = "${pkgs.rocmPackages.rocm-smi}/bin/rocm-smi";
          "timeout" = "5s";
        };
        "exec" = {
          "name_override" = "NZXT E500";
          "interval" = "2s";
          "timeout" = "5s";
          "commands" = [ "${pkgs.liquidctl}/bin/liquidctl status -m 'E500' --json" ];
          "data_format" = "json_v2";
          "json_v2" = [{
            "measurement_name" = "valix_psu"; # need this line to ensure "json_v2" doesn't get merged into "json_v2.object" ¯\_(ツ)_/¯ (since ~2023-01-21)
            "object" = [{
              "path" = "#.status";
              "tags" = [ "key" ];
            }];
          }];
        };
      };
    };
  };

  systemd.services.telegraf.path = [ pkgs.lm_sensors ];
  services.telegraf.extraConfig."inputs"."sensors" = { };
}
